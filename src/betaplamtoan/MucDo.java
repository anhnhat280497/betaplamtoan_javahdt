/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package betaplamtoan;

/**
 *
 * @author MyPC
 */
public class MucDo {
    private int min ;
    private int max ;
    
    public int getMin(){
        return min;
    }
    public void setMin(int Min){
        min = Min;
    }
    public int getMax(){
        return max;
    }
    public void setMax(int Max){
        max = Max;
    }
    public MucDo(){
        min = 0;
        max = 0;
    }
    public MucDo(int Min, int Max){
        min = Min; max = Max;
    }
    public MucDo(MucDo mucdo){
        min = mucdo.min;
        max = mucdo.max;
    }
}
