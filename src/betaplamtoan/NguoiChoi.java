/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package betaplamtoan;

/**
 *
 * @author MyPC
 */
public class NguoiChoi {
    private String Name;
    private int Diem = 0;
    
    public void setName(String name){
        Name = name;
    }
    public String getName(){
        return Name;
    }
    public void setDiem(int diem){
        Diem = diem;
    }
    public int getDiem(){
        return Diem;
    }
    public NguoiChoi(){
        Name = "";
        Diem = 0;
    }
    public NguoiChoi(String name, int diem){
        Name = name;
        Diem = diem;
    }
    public NguoiChoi(NguoiChoi a){
        Name = a.Name;
        Diem = a.Diem;
    }
    
}
