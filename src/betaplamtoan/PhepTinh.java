
package betaplamtoan;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import javax.swing.Timer;

public class PhepTinh extends javax.swing.JFrame{

    private CauHoi cauhoi;
    private NguoiChoi NewPlayer, BestPlayer; 
    private Timer time; //Điều khiển cột thời gian
    private MucDo mucdo; // Mức độ khó dễ của các số random
    private int ViTriDA, StartNewGame=0, DemTime = 100, CacDapAn[];
    public PhepTinh() {
        initComponents();
        hienBatDau(false);
    }
    
    public void hienBatDau(boolean t){
        nhapTen.setVisible(t);
        nhapTenNguoiChoi.setVisible(t);
        mucDe.setVisible(t);
        mucKho.setVisible(t);
        mucVua.setVisible(t);
        choiMoi.setVisible(t);
        batDauChoi.setVisible(!t);
    }
    
    public class LoadTime implements ActionListener{ 
        @Override
        public void actionPerformed(ActionEvent e) {
            if(DemTime>0){
                DemTime--;
                ThoiGian.setValue(DemTime);
            }
            else {
                thua();
            }
        }        
    }
    public void thua(){
        time.stop();
        if(NewPlayer.getDiem() > BestPlayer.getDiem()) { //Nếu điểm vừa chơi lớn hơn kỷ lục
            thua.setText("KỶ LỤC MỚI!"); // Hiện thông báo kỷ lục mới
            BestPlayer.setDiem(NewPlayer.getDiem()); BestPlayer.setName(nhapTenNguoiChoi.getText()) ; // set điểm và tên người chơi mới            
            
            tenKyLuc.setText(BestPlayer.getName()); // hiện tên và điểm lên các mục điểm kỷ lục và tên người chơi
            diemKyLuc.setText(BestPlayer.getDiem() + "");
  
            ghiFile(); //Ghi dữ liệu của kỷ lục mới vào File
        }
        else thua.setText("THUA!"); //Điểm vừa chơi thấp hơn kỷ lục thì thông báo THUA.
        phepToan.setText("ĐIỂM: "+ NewPlayer.getDiem()); //Hiện điểm vừa chơi 
        hienDapAn(false);
    }
    
    public void setDapAn(){
        dapAn1.setText(CacDapAn[0]+" "); //Hiện các đáp án được ramdom
        dapAn2.setText(CacDapAn[1]+" ");
        dapAn3.setText(CacDapAn[2]+" ");
        dapAn4.setText(CacDapAn[3]+" ");
        dapAn5.setText(CacDapAn[4]+" ");
        dapAn6.setText(CacDapAn[5]+" ");
        dapAn7.setText(CacDapAn[6]+" ");
        dapAn8.setText(CacDapAn[7]+" ");
        dapAn9.setText(CacDapAn[8]+" ");
        int DapAn = cauhoi.getSoA()+cauhoi.getSoB();
        switch(ViTriDA){ // Hiện đáp án đúng ở vị trí đã được random
            case 1: { CacDapAn[0] = DapAn; dapAn1.setText(DapAn+""); break;}
            case 2: { CacDapAn[1] = DapAn; dapAn2.setText(DapAn+""); break;}
            case 3: { CacDapAn[2] = DapAn; dapAn3.setText(DapAn+""); break; }
            case 4: { CacDapAn[3] = DapAn; dapAn4.setText(DapAn+""); break; }
            case 5: { CacDapAn[4] = DapAn; dapAn5.setText(DapAn+""); break;}
            case 6: { CacDapAn[5] = DapAn; dapAn6.setText(DapAn+""); break;}
            case 7: { CacDapAn[6] = DapAn; dapAn7.setText(DapAn+""); break;}
            case 8: { CacDapAn[7] = DapAn; dapAn8.setText(DapAn+""); break;}
            case 9: { CacDapAn[8] = DapAn; dapAn9.setText(DapAn+""); break;}
        }
    }
    
    public void random(){ //Thực hiện random CÂU HỎI, ĐÁP ÁN ĐÚNG, CÁC ĐÁP ÁN NGẪU NHIÊN, random Vị trí xuất hiện của đáp án
        Random rd = new Random();
        cauhoi.setSoA( rd.nextInt(mucdo.getMax()) + mucdo.getMin()); // Số A nằm trong khoảng từ b đến a
        cauhoi.setSoB( rd.nextInt(mucdo.getMax()) + mucdo.getMin());
        ViTriDA = rd.nextInt(9) +1;      //Vị trí của đáp án đúng
        int DapAn = cauhoi.getSoA()+cauhoi.getSoB(), KiemTraTrung; 
        phepToan.setText(cauhoi.getSoA() + " + " + cauhoi.getSoB() + " = ?"); // Hiện câu hỏi vừa random
        for (int i = 0; i < 9; i++ ) {  // Kiểm tra các đáp án ngẫu nhiên trùng nhau 
            do{                
                CacDapAn[i] = rd.nextInt(mucdo.getMax()*2)+1;
                KiemTraTrung = 1;
                for(int j=0; j<i; j++) if(CacDapAn[j]==CacDapAn[i]) KiemTraTrung = 0;
            } while(KiemTraTrung==0 || CacDapAn[i]==DapAn);
        } 
        setDapAn();
    }
    public void ghiFile(){
        try{ // Ghi dữ liệu của điểm và tên, phân cách bởi dấu phẩy
            FileWriter out = new FileWriter("High.txt");
            out.write(String.valueOf(BestPlayer.getDiem()));
            out.write(",");
            out.write(BestPlayer.getName());
            out.close();
        }
        catch(IOException e){
            System.out.println(e);
        }
    }
    public void docFile(){
        try{
            FileReader in = new FileReader("High.txt");
            int data, diem = 0; String name = ""; // đọc file ra biến name, sau đó cắt chuỗi
            while((data = in.read()) != -1)     name+= (char)data; 
            in.close();
            for(int i = 0;  i<name.length(); i++){
                if(name.charAt(i) == 44) { // nếu là dấu phẩy
                    name = name.substring(i+1); // cắt chuỗi tại i+1 là sau dấu phẩy
                    break;
                }
                diem = diem*10 + ((int)name.charAt(i) - 48); // tách điểm từ name
            }
            BestPlayer.setName(name); // set giá trị kỷ lục
            BestPlayer.setDiem(diem);
            nhapTenNguoiChoi.setText(BestPlayer.getName()+""); //hiện tên người chơi đạt kỷ lục ở mục nhập tên khi bắt đầu chơi
        }                                          // có thể sửa lại, hoặc để mặc định
        catch(IOException e){
            System.out.println(e);
        }
    }
    
    public void chonDapAn(int i){
        if((cauhoi.getSoA()+cauhoi.getSoB())==CacDapAn[i])     { // nếu đáp án nằm ở ô số i
            int Diem = NewPlayer.getDiem(); // khởi tạo biến điểm
            random(); 
            diemMoi.setText(++Diem+""); // random câu hỏi tiếp theo, cộng điểm lên 1
            NewPlayer.setDiem(Diem);    // set điểm
            DemTime =100;       // đặt lại cột thời gian
            time.start();       // bắt đầu tính thời gian cho câu hỏi mới
        }
        else thua(); // nếu không phải đáp án thì kết thúc
    }
    public void chonMucDo(int min, int max, int tg){
        mucdo.setMin(min); mucdo.setMax(max); // thiết lập phạm vi random và thời gian
        time = new Timer(tg, new LoadTime());
    }
    public void hienDapAn(boolean t){
        dapAn1.setVisible(t); // Hiện các ô đáp án để người chơi lựa chọn
        dapAn2.setVisible(t);
        dapAn3.setVisible(t);
        dapAn4.setVisible(t);
        dapAn5.setVisible(t);
        dapAn6.setVisible(t);
        dapAn7.setVisible(t);
        dapAn8.setVisible(t);
        dapAn9.setVisible(t);
        thoat.setVisible(!t); // không cho thoát khi đang chơi, ẩn các ô chọn mức dộ, chơi mới,...
        mucDe.setVisible(!t);
        mucKho.setVisible(!t);
        mucVua.setVisible(!t);
        choiMoi.setVisible(!t);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        tenGame = new javax.swing.JLabel();
        ThoiGian = new javax.swing.JProgressBar();
        phepToan = new javax.swing.JLabel();
        dapAn1 = new javax.swing.JButton();
        dapAn3 = new javax.swing.JButton();
        dapAn2 = new javax.swing.JButton();
        dapAn4 = new javax.swing.JButton();
        dapAn5 = new javax.swing.JButton();
        dapAn6 = new javax.swing.JButton();
        choiMoi = new javax.swing.JButton();
        dapAn8 = new javax.swing.JButton();
        dapAn9 = new javax.swing.JButton();
        dapAn7 = new javax.swing.JButton();
        mucDe = new javax.swing.JRadioButton();
        mucVua = new javax.swing.JRadioButton();
        mucKho = new javax.swing.JRadioButton();
        thua = new javax.swing.JLabel();
        thoat = new javax.swing.JButton();
        nhapTenNguoiChoi = new javax.swing.JTextField();
        tenMoi = new javax.swing.JLabel();
        batDauChoi = new javax.swing.JButton();
        tenKyLuc = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nhapTen = new javax.swing.JLabel();
        diemKyLuc = new javax.swing.JLabel();
        diemMoi = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);

        tenGame.setFont(new java.awt.Font("UTM American Sans", 1, 36)); // NOI18N
        tenGame.setForeground(new java.awt.Color(0, 153, 255));
        tenGame.setText("LUYỆN PHÉP CỘNG");
        tenGame.setToolTipText("");

        phepToan.setFont(new java.awt.Font("Times New Roman", 0, 48)); // NOI18N
        phepToan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        phepToan.setText("Câu Hỏi");
        phepToan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        phepToan.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        dapAn1.setText("0");
        dapAn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn1ActionPerformed(evt);
            }
        });

        dapAn3.setText("0");
        dapAn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn3ActionPerformed(evt);
            }
        });

        dapAn2.setText("0");
        dapAn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn2ActionPerformed(evt);
            }
        });

        dapAn4.setText("0");
        dapAn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn4ActionPerformed(evt);
            }
        });

        dapAn5.setText("0");
        dapAn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn5ActionPerformed(evt);
            }
        });

        dapAn6.setText("0");
        dapAn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn6ActionPerformed(evt);
            }
        });

        choiMoi.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        choiMoi.setText("CHƠI MỚI");
        choiMoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choiMoiActionPerformed(evt);
            }
        });

        dapAn8.setText("0");
        dapAn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn8ActionPerformed(evt);
            }
        });

        dapAn9.setText("0");
        dapAn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn9ActionPerformed(evt);
            }
        });

        dapAn7.setText("0");
        dapAn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dapAn7ActionPerformed(evt);
            }
        });

        buttonGroup1.add(mucDe);
        mucDe.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        mucDe.setText("Dễ");
        mucDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mucDeActionPerformed(evt);
            }
        });

        buttonGroup1.add(mucVua);
        mucVua.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        mucVua.setSelected(true);
        mucVua.setText("Vừa");
        mucVua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mucVuaActionPerformed(evt);
            }
        });

        buttonGroup1.add(mucKho);
        mucKho.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        mucKho.setText("Khó");
        mucKho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mucKhoActionPerformed(evt);
            }
        });

        thua.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        thua.setForeground(new java.awt.Color(255, 0, 0));
        thua.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        thua.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(51, 51, 255)));
        thua.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        thoat.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        thoat.setText("THOÁT");
        thoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thoatActionPerformed(evt);
            }
        });

        nhapTenNguoiChoi.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        nhapTenNguoiChoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nhapTenNguoiChoiActionPerformed(evt);
            }
        });

        tenMoi.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        tenMoi.setForeground(new java.awt.Color(255, 0, 51));
        tenMoi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tenMoi.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 0, 153)));

        batDauChoi.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        batDauChoi.setText("BẮT ĐẦU CHƠI");
        batDauChoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batDauChoiActionPerformed(evt);
            }
        });

        tenKyLuc.setFont(new java.awt.Font("Times New Roman", 1, 16)); // NOI18N
        tenKyLuc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tenKyLuc.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 0, 153)));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 0));
        jLabel1.setText("KỶ LỤC:");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 0));
        jLabel3.setText("ĐIỂM:");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setText("TÊN:");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 0));
        jLabel5.setText("TÊN:");

        nhapTen.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        nhapTen.setForeground(new java.awt.Color(0, 102, 0));
        nhapTen.setText("TÊN NGƯỜI CHƠI:");

        diemKyLuc.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        diemKyLuc.setForeground(new java.awt.Color(255, 0, 0));
        diemKyLuc.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        diemKyLuc.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 0, 0)));

        diemMoi.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        diemMoi.setForeground(new java.awt.Color(255, 0, 0));
        diemMoi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        diemMoi.setText("0");
        diemMoi.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 0, 0)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ThoiGian, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(phepToan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(thua, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tenKyLuc, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tenMoi, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(diemKyLuc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(diemMoi, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(84, Short.MAX_VALUE)
                .addComponent(tenGame)
                .addGap(82, 82, 82))
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(choiMoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(batDauChoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(thoat, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 145, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(nhapTen)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nhapTenNguoiChoi)
                                .addGap(66, 66, 66)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dapAn4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dapAn1, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                            .addComponent(dapAn7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(dapAn5, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(63, 63, 63))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dapAn2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dapAn8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dapAn6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dapAn3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dapAn9, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(66, 66, 66))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(mucDe)
                        .addGap(110, 110, 110)
                        .addComponent(mucVua)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mucKho)
                        .addGap(80, 80, 80))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(tenGame)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ThoiGian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tenKyLuc, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(diemKyLuc, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(tenMoi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(diemMoi, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(thua, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(phepToan, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dapAn3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dapAn2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dapAn4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dapAn6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dapAn5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(dapAn1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dapAn9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(dapAn7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dapAn8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nhapTen)
                    .addComponent(nhapTenNguoiChoi, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mucDe)
                    .addComponent(mucVua)
                    .addComponent(mucKho))
                .addGap(11, 11, 11)
                .addComponent(batDauChoi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(choiMoi)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(thoat)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dapAn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn7ActionPerformed
        chonDapAn(6);
    }//GEN-LAST:event_dapAn7ActionPerformed

    private void dapAn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn6ActionPerformed
        chonDapAn(5);
    }//GEN-LAST:event_dapAn6ActionPerformed

    private void dapAn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn8ActionPerformed
        chonDapAn(7);
    }//GEN-LAST:event_dapAn8ActionPerformed

    private void dapAn9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn9ActionPerformed
        chonDapAn(8);
    }//GEN-LAST:event_dapAn9ActionPerformed

    private void dapAn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn1ActionPerformed
        chonDapAn(0);
    }//GEN-LAST:event_dapAn1ActionPerformed

    private void choiMoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choiMoiActionPerformed
        if(StartNewGame==1){ // Khi nhấn Bắt đầu chơi mới được thực hiện
            hienDapAn(true);          
            nhapTen.setVisible(false);
            nhapTenNguoiChoi.setVisible(false);
            tenMoi.setText(nhapTenNguoiChoi.getText());  // Tên người chơi mới lấy từ ô nhập tên 
            thua.setText(""); // xóa thông báo
            DemTime =100; NewPlayer.setDiem(0); diemMoi.setText(0+""); //thiết lập lại thời gian, điểm
            random(); // tạo câu hỏi
            time.start(); // bắt đầu chơi
        }
    }//GEN-LAST:event_choiMoiActionPerformed

    private void dapAn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn2ActionPerformed
        chonDapAn(1);
    }//GEN-LAST:event_dapAn2ActionPerformed

    private void dapAn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn3ActionPerformed
        chonDapAn(2);
    }//GEN-LAST:event_dapAn3ActionPerformed

    private void dapAn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn4ActionPerformed
        chonDapAn(3);
    }//GEN-LAST:event_dapAn4ActionPerformed

    private void dapAn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dapAn5ActionPerformed
        chonDapAn(4);
    }//GEN-LAST:event_dapAn5ActionPerformed

    private void mucVuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mucVuaActionPerformed
        chonMucDo(1, 25, 50);
    }//GEN-LAST:event_mucVuaActionPerformed

    private void mucKhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mucKhoActionPerformed
        chonMucDo(1, 30, 40);
    }//GEN-LAST:event_mucKhoActionPerformed

    private void mucDeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mucDeActionPerformed
        chonMucDo(1, 20, 100);
    }//GEN-LAST:event_mucDeActionPerformed

    private void thoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_thoatActionPerformed
        System.exit(0);
    }//GEN-LAST:event_thoatActionPerformed

    private void nhapTenNguoiChoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nhapTenNguoiChoiActionPerformed
        tenMoi.setText(nhapTenNguoiChoi.getText());     // tên người chơi mới
    }//GEN-LAST:event_nhapTenNguoiChoiActionPerformed

    private void batDauChoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batDauChoiActionPerformed
        StartNewGame=1; // Điều kiện chơi mới
        CacDapAn =  new int[9];
        hienBatDau(true);
        cauhoi = new CauHoi();
        NewPlayer = new NguoiChoi();
        BestPlayer = new NguoiChoi();
        mucdo = new MucDo();
        chonMucDo(1, 25, 50); // mặc định là mức vừa
        docFile(); // đọc dữ liệu kỷ lục
        tenKyLuc.setText(BestPlayer.getName()); // Hiện tên và điểm kỷ lục
        diemKyLuc.setText(BestPlayer.getDiem() + ""); //
    }//GEN-LAST:event_batDauChoiActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PhepTinh.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PhepTinh.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PhepTinh.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PhepTinh.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PhepTinh().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar ThoiGian;
    private javax.swing.JButton batDauChoi;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton choiMoi;
    private javax.swing.JButton dapAn1;
    private javax.swing.JButton dapAn2;
    private javax.swing.JButton dapAn3;
    private javax.swing.JButton dapAn4;
    private javax.swing.JButton dapAn5;
    private javax.swing.JButton dapAn6;
    private javax.swing.JButton dapAn7;
    private javax.swing.JButton dapAn8;
    private javax.swing.JButton dapAn9;
    private javax.swing.JLabel diemKyLuc;
    private javax.swing.JLabel diemMoi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButton mucDe;
    private javax.swing.JRadioButton mucKho;
    private javax.swing.JRadioButton mucVua;
    private javax.swing.JLabel nhapTen;
    private javax.swing.JTextField nhapTenNguoiChoi;
    private javax.swing.JLabel phepToan;
    private javax.swing.JLabel tenGame;
    private javax.swing.JLabel tenKyLuc;
    private javax.swing.JLabel tenMoi;
    private javax.swing.JButton thoat;
    private javax.swing.JLabel thua;
    // End of variables declaration//GEN-END:variables
}
