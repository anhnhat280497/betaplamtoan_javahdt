
package betaplamtoan;

public class CauHoi {
    private int SoA, SoB;
    
    public void setSoA(int A){
        SoA = A;
    }
    public int getSoA(){
        return SoA;
    }
    public void setSoB(int B){
        SoB = B;
    }
    public int getSoB(){
        return SoB;
    }
    public CauHoi(){
        SoA = 0; SoB = 0;
    }
    public CauHoi(int A, int B){
        SoA = A; SoB = B;
    }
    public CauHoi(CauHoi c){
        SoA = c.SoA; SoB = c.SoB;
    }
}
